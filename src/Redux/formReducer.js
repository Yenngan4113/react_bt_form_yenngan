import {
  ADD_NEW_STUDENT,
  DEL_STUDENT,
  EDIT_STUDENT,
  GET_INFOR,
  GET_SEARCH_STUDENT,
  SEARCH_STUDENT,
  UPDATE_STUDENT,
} from "./constant";

const initialState = {
  formInfor: {
    maSV: "",
    phoneNumber: "",
    fullName: "",
    email: "",
  },
  studentList: [],
  isDisplay: "none",
  isDisabled: false,
  notice: "",
  noticeSearch: "",
  searchName: "",
  searchList: [],
};

export const formReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case GET_INFOR: {
      state.formInfor = payload;
      let dataJson = JSON.stringify(state);
      let stateData = JSON.parse(dataJson);
      return { ...stateData };
    }
    case ADD_NEW_STUDENT: {
      let cloneStudentList = [...state.studentList];
      let index = cloneStudentList.findIndex((listItem) => {
        return listItem.maSV == payload.maSV;
      });

      if (payload.maSV != "" && index == -1) {
        cloneStudentList.push(payload);
        state.notice = "";
      } else if (payload.maSV == "") {
        state.notice = "Không được để trống mã sinh viên";
      } else {
        state.notice = "Không được trùng mã sinh viên";
      }
      state.studentList = cloneStudentList;
      let dataJson = JSON.stringify(state);
      let stateData = JSON.parse(dataJson);
      return { ...stateData };
    }
    case DEL_STUDENT: {
      let cloneStudentList = [...state.studentList];
      let index = cloneStudentList.findIndex((listItem) => {
        return listItem.maSV == payload.maSV;
      });

      if (index != -1) {
        cloneStudentList.splice(index, 1);
      }
      state.studentList = cloneStudentList;

      let dataJson = JSON.stringify(state);
      let stateData = JSON.parse(dataJson);
      return { ...stateData };
    }
    case EDIT_STUDENT: {
      state.formInfor = payload;
      state.isDisplay = "inline-block";
      state.isDisabled = true;
      let dataJson = JSON.stringify(state);
      let stateData = JSON.parse(dataJson);
      return { ...stateData };
    }
    case UPDATE_STUDENT: {
      let cloneStudentList = [...state.studentList];
      let index = cloneStudentList.findIndex((listItem) => {
        return listItem.maSV == payload.maSV;
      });
      if (index != -1) {
        cloneStudentList[index] = payload;
      }
      state.isDisabled = false;

      state.studentList = cloneStudentList;
      state.isDisplay = "none";
      let dataJson = JSON.stringify(state);
      let stateData = JSON.parse(dataJson);
      return { ...stateData };
    }
    case GET_SEARCH_STUDENT: {
      state.searchName = payload;
      let cloneSearchList = [...state.searchList];
      cloneSearchList.forEach((listItem) => {
        if (listItem.fullName == payload.fullName) {
          cloneSearchList.push(listItem);
        }
      });
      state.searchList = cloneSearchList;
      if (state.searchName == "") {
        state.searchList = [];
        state.noticeSearch = "";
      }
      let dataJson = JSON.stringify(state);
      let stateData = JSON.parse(dataJson);
      return { ...stateData };
    }
    case SEARCH_STUDENT: {
      let cloneSearchList = [...state.searchList];
      cloneSearchList = state.studentList.filter((listItem) => {
        if (listItem.fullName == payload) {
          return listItem;
        }
      });
      if (cloneSearchList.length == 0) {
        state.noticeSearch = "Không tìm thấy sinh viên";
      }
      state.searchList = cloneSearchList;
      let dataJson = JSON.stringify(state);
      let stateData = JSON.parse(dataJson);
      return { ...stateData };
    }
    default:
      return state;
  }
};

import logo from "./logo.svg";
import "antd/dist/antd.css";
import "./App.css";
import { Fragment } from "react";
import FormBT from "./Component/Form";

function App() {
  return (
    <Fragment>
      <FormBT />
    </Fragment>
  );
}

export default App;

export const GET_INFOR = "Get infor from form";
export const ADD_NEW_STUDENT = "Add new student to studentList";
export const DEL_STUDENT = "Del student from studentList";
export const EDIT_STUDENT = "EDIT student from studentList";
export const UPDATE_STUDENT = "UPDATE student from studentList";
export const GET_SEARCH_STUDENT = "Get student name from search input";
export const SEARCH_STUDENT = "Search student from studentList";

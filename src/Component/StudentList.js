import { Button, Input, List } from "antd";
import React, { Component } from "react";
import { connect } from "react-redux";
import {
  DEL_STUDENT,
  EDIT_STUDENT,
  GET_INFOR,
  GET_SEARCH_STUDENT,
  SEARCH_STUDENT,
} from "../Redux/constant";

const { Search } = Input;

class StudentList extends Component {
  //   shouldComponentUpdate(nextProps, nextState) {
  //     console.log(nextProps);
  // this.props.List;
  // console.log("this.props.List: ", this.props.List);
  //   }

  handleChangeSearch = (e) => {
    let value = e.target.value;
    this.props.searchName(value);
  };
  handleSearch = () => {
    this.props.handleSearch(this.props.SearchName);
  };

  render() {
    return (
      <div className="flex flex-col mx-24 ">
        <div className=" self-end w-1/2">
          <Search
            placeholder="Nhập tên sinh viên"
            value={this.props.SearchName}
            name="fullName"
            allowClear
            enterButton="Search"
            size="large"
            style={{ width: "100%", display: "block" }}
            onSearch={() => {
              this.handleSearch();
            }}
            onChange={(e) => {
              this.handleChangeSearch(e);
            }}
          />
          <span id="searchNoti" className=" self-end text-red-600">
            {this.props.NoticeSearch}
          </span>
        </div>
        <table className="table text-center mt-8 ">
          <thead className=" bg-black text-white text-lg ">
            <tr>
              <td>Mã sinh viên</td>
              <td>Họ tên</td>
              <td>Số điện thoại</td>
              <td>Email</td>
              <td></td>
            </tr>
          </thead>
          <tbody>
            {this.props.SearchList.length == 0 &&
              this.props.List.map((item) => {
                return (
                  <tr>
                    <td>{item.maSV}</td>
                    <td>{item.fullName}</td>
                    <td>{item.phoneNumber}</td>
                    <td>{item.email}</td>
                    <td>
                      <Button
                        type="primary"
                        danger
                        onClick={() => {
                          this.props.handleDelete(item);
                        }}
                      >
                        Xóa
                      </Button>
                      <Button
                        style={{ backgroundColor: "green" }}
                        className=" text-white"
                        onClick={() => {
                          this.props.handleEdit(item);
                        }}
                      >
                        Sửa
                      </Button>
                    </td>
                  </tr>
                );
              })}

            {this.props.SearchList.length != 0 &&
              this.props.SearchList.map((item) => {
                return (
                  <tr>
                    <td>{item.maSV}</td>
                    <td>{item.fullName}</td>
                    <td>{item.phoneNumber}</td>
                    <td>{item.email}</td>
                    <td>
                      <Button
                        type="primary"
                        danger
                        onClick={() => {
                          this.props.handleDelete(item);
                        }}
                      >
                        Xóa
                      </Button>
                      <Button
                        style={{ backgroundColor: "green" }}
                        className=" text-white"
                        onClick={() => {
                          this.props.handleEdit(item);
                        }}
                      >
                        Sửa
                      </Button>
                    </td>
                  </tr>
                );
              })}
          </tbody>
        </table>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    List: state.studentList,
    Infor: state.formInfor,
    SearchName: state.searchName,
    SearchList: state.searchList,
    NoticeSearch: state.noticeSearch,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    handleDelete: (item) => {
      dispatch({
        type: DEL_STUDENT,
        payload: item,
      });
    },
    handleEdit: (item) => {
      dispatch({
        type: EDIT_STUDENT,
        payload: item,
      });
    },
    searchName: (item) => {
      dispatch({
        type: GET_SEARCH_STUDENT,
        payload: item,
      });
    },
    handleSearch: (item) => {
      dispatch({
        type: SEARCH_STUDENT,
        payload: item,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(StudentList);

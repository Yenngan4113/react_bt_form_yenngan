import React, { Component } from "react";
import { Button, Form, Input, InputNumber } from "antd";
import { connect } from "react-redux";
import {
  ADD_NEW_STUDENT,
  EDIT_STUDENT,
  GET_INFOR,
  UPDATE_STUDENT,
} from "../Redux/constant";
import StudentList from "./StudentList";

class FormBT extends Component {
  handleChangeForm = (e) => {
    let value = e.target.value;
    let name = e.target.name;
    let infor = { ...this.props.Infor, [name]: value };
    this.props.getInfor(infor);
  };

  handleSummit = (e) => {
    let infor = {
      maSV: "",
      phoneNumber: "",
      fullName: "",
      email: "",
    };

    this.props.handleAddSV(this.props.Infor);
    this.props.getInfor(infor);
  };
  handleEdit = (e) => {
    let infor = {
      maSV: "",
      phoneNumber: "",
      fullName: "",
      email: "",
    };

    this.props.handleUpdateSV(this.props.Infor);
    this.props.getInfor(infor);
  };
  // shouldComponentUpdate = (nextProps) => {
  //   console.log(nextProps);
  //   // this.props.showEditSV(nextProps.Infor);
  // };
  render() {
    return (
      <div>
        <div className=" h-max py-6 px-24 text-3xl  bg-black text-white text font-medium">
          THÔNG TIN SINH VIÊN
        </div>
        <form>
          <div className=" mx-24 my-8">
            <div className="row g-3">
              <div className="col">
                <label for="inputEmail4" class="form-label">
                  Mã sinh viên
                </label>
                <input
                  value={this.props.Infor.maSV}
                  type="text"
                  name="maSV"
                  className="form-control"
                  placeholder="Mã sinh viên"
                  aria-label="First name"
                  disabled={this.props.Disabled}
                  onChange={(e) => {
                    this.handleChangeForm(e);
                  }}
                />
                <span className=" text-red-500">{this.props.Notice}</span>
              </div>
              <div className="col">
                <label for="inputEmail4" class="form-label">
                  Họ tên
                </label>
                <input
                  value={this.props.Infor.fullName}
                  type="text"
                  name="fullName"
                  className="form-control"
                  placeholder="Họ tên"
                  aria-label="Last name"
                  onChange={(e) => {
                    this.handleChangeForm(e);
                  }}
                />
              </div>
            </div>
            <div className="row g-3 my-8">
              <div className="col">
                <label for="inputEmail4" class="form-label">
                  Số điện thoại
                </label>
                <input
                  value={this.props.Infor.phoneNumber}
                  type="text"
                  name="phoneNumber"
                  className="form-control"
                  placeholder="Số điện thoại"
                  aria-label="First name"
                  onChange={(e) => {
                    this.handleChangeForm(e);
                  }}
                />
              </div>
              <div className="col">
                <label for="inputEmail4" class="form-label">
                  Email
                </label>
                <input
                  value={this.props.Infor.email}
                  type="text"
                  name="email"
                  className="form-control"
                  placeholder="Email"
                  aria-label="Last name"
                  onChange={(e) => {
                    this.handleChangeForm(e);
                  }}
                />
              </div>
            </div>
          </div>
          <div id="buttonPosition">
            <Button
              type="primary"
              className="mx-24 mb-8 max-h-min "
              onClick={(e) => {
                this.handleSummit(e);
              }}
            >
              <span>Thêm sinh viên</span>
            </Button>
            <Button
              id="positionAb"
              style={{
                backgroundColor: "#FFA500",
                display: `${this.props.Display}`,
              }}
              className="mx-24 mb-8 max-h-min text-white"
              onClick={(e) => {
                this.handleEdit(e);
              }}
            >
              <span>Cập nhật sinh viên</span>
            </Button>
          </div>
        </form>
        <StudentList />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    Infor: state.formInfor,
    StudentList: state.studentList,
    Display: state.isDisplay,
    Disabled: state.isDisabled,
    Notice: state.notice,
    SearchList: state.searchList,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    getInfor: (data) => {
      dispatch({
        type: GET_INFOR,
        payload: data,
      });
    },
    handleAddSV: (data) => {
      dispatch({
        type: ADD_NEW_STUDENT,
        payload: data,
      });
    },
    handleUpdateSV: (data) => {
      dispatch({
        type: UPDATE_STUDENT,
        payload: data,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(FormBT);
